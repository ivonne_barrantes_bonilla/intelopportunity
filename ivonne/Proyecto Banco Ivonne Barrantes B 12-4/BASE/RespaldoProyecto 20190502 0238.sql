-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.4.3-beta-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema proyecto
--

CREATE DATABASE IF NOT EXISTS proyecto;
USE proyecto;

--
-- Definition of table `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
CREATE TABLE `bitacora` (
  `NUMEROMOVIMIENTO` int(10) NOT NULL AUTO_INCREMENT,
  `FECHAMOVIMIENTO` varchar(45) NOT NULL,
  `USUARIO` varchar(45) NOT NULL,
  `TRANSACCIONREALIZADA` varchar(45) NOT NULL,
  PRIMARY KEY (`NUMEROMOVIMIENTO`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bitacora`
--

/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` (`NUMEROMOVIMIENTO`,`FECHAMOVIMIENTO`,`USUARIO`,`TRANSACCIONREALIZADA`) VALUES 
 (25,'21/04/2019','null','Registrar usuarios'),
 (26,'21/04/2019','null','Registrar Cliente'),
 (27,'21/04/2019','null','Registrar Cliente'),
 (28,'','null','Modificar Cliente'),
 (29,'','null','Modificar Cliente'),
 (30,'','null','Modificar Cliente'),
 (31,'','null','Modificar Cliente'),
 (32,'21/04/2019','null','Registrar Sucursales'),
 (33,'21/04/2019','null','Registrar Sucursales'),
 (34,'21/04/2019','null','Modificar Sucursales'),
 (35,'21/04/2019','null','Modificar Sucursales'),
 (36,'21/04/2019','null','Modificar Sucursales'),
 (37,'21/04/2019','null','Modificar Sucursales'),
 (38,'21/04/2019','null','Modificar Sucursales'),
 (39,'21/04/2019','null','Modificar Sucursales'),
 (40,'21/04/2019','null','Modificar Sucursales'),
 (41,'21/04/2019','null','Modificar Sucursales'),
 (42,'21/04/2019','null','Modificar Sucursales'),
 (43,'21/04/2019','null','Modificar Sucursales'),
 (44,'21/04/2019','null','Modificar Sucursales'),
 (45,'21/04/2019','null','Registrar Sucursales'),
 (46,'21/04/2019','null','Registrar Sucursales'),
 (47,'21/04/2019','null','Modificar Sucursales'),
 (48,'21/04/2019','null','Modificar Sucursales'),
 (49,'21/04/2019','null','Modificar Sucursales'),
 (50,'21/04/2019','null','Modificar Sucursales'),
 (51,'21/04/2019','null','Registrar Cuentas'),
 (52,'21/04/2019','null','Registrar Cuentas'),
 (53,'21/04/2019','null','Activar/Desactivar Cuenta'),
 (54,'21/04/2019','null','Registrar Transacciones'),
 (55,'21/04/2019','null','Registrar Transacciones'),
 (56,'21/04/2019','null',''),
 (57,'21/04/2019','root','Iniciar Sesion'),
 (58,'21/04/2019','root','Iniciar Sesion'),
 (59,'21/04/2019','root','Iniciar Sesion'),
 (60,'21/04/2019','root','Iniciar Sesion'),
 (61,'21/04/2019','root','Iniciar Sesion'),
 (62,'21/04/2019','null','Registrar Nivel'),
 (63,'22/04/2019','null','Registrar Transacciones'),
 (64,'22/04/2019','null','Activar/Desactivar Cuenta'),
 (65,'22/04/2019','null','Activar/Desactivar Cuenta'),
 (66,'22/04/2019','null','Registrar Transacciones'),
 (67,'22/04/2019','null',''),
 (68,'22/04/2019','null',''),
 (69,'24/04/2019','root','Iniciar Sesion'),
 (70,'24/04/2019','null','Registrar Transacciones'),
 (71,'','null','Eliminando Cuentas'),
 (72,'24/04/2019','null','Registrar Cuentas'),
 (73,'24/04/2019','null','Registrar Cuentas'),
 (74,'24/04/2019','null','Registrar Cuentas'),
 (75,'24/04/2019','null','Activar/Desactivar Cuenta'),
 (76,'24/04/2019','null','Registrar Transacciones'),
 (77,'24/04/2019','null','Registrar Transacciones'),
 (78,'24/04/2019','null','Registrar Transacciones'),
 (79,'24/04/2019','null','Registrar Transacciones'),
 (80,'24/04/2019','null','Registrar Transacciones'),
 (81,'24/04/2019','null','Registrar Cuentas'),
 (82,'24/04/2019','null','Registrar Nivel'),
 (83,'1/05/2019','root','Iniciar Sesion'),
 (84,'1/05/2019','root','Iniciar Sesion'),
 (85,'01/05/2019','root','Registrar Cliente'),
 (86,'1/05/2019','root','Iniciar Sesion'),
 (87,'1/05/2019','null','Registrar Nivel'),
 (88,'1/05/2019','null','Registrar Nivel'),
 (89,'2/05/2019','root','Iniciar Sesion'),
 (90,'2/05/2019','root','Iniciar Sesion'),
 (91,'2/05/2019','root','Iniciar Sesion'),
 (92,'2/05/2019','root','Iniciar Sesion');
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;


--
-- Definition of table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `IDENTIFICACION` varchar(20) NOT NULL,
  `TELEFONO` varchar(25) NOT NULL,
  `DIRECCION` varchar(45) NOT NULL,
  `CORREO` varchar(45) NOT NULL,
  `FECHAREGISTRO` varchar(45) NOT NULL,
  `CODIGOSUCURSAL` varchar(45) NOT NULL,
  `NOMBRECLIENTE` varchar(45) NOT NULL,
  PRIMARY KEY (`IDENTIFICACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`IDENTIFICACION`,`TELEFONO`,`DIRECCION`,`CORREO`,`FECHAREGISTRO`,`CODIGOSUCURSAL`,`NOMBRECLIENTE`) VALUES 
 ('123','89087365','Guadalupe ','hbnivn05@gmail.com','21/04/2019','1','Hedy'),
 ('234','84030346','Guadalupe ','bivonneb16@gmail.com','21/04/2019','1','Ivonne'),
 ('456','89094567','Heredia','maria@yahoo.com','01/05/2019','1','Maria');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;


--
-- Definition of table `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE `cuentas` (
  `NUMEROCUENTA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FECHAAPERTURA` varchar(45) NOT NULL,
  `TIPOCUENTA` varchar(45) NOT NULL,
  `TIPOMONEDA` varchar(45) NOT NULL,
  `SALDOCUENTA` double NOT NULL,
  `IDENTIFICACION` varchar(45) NOT NULL,
  `MONTOAPERTURA` double NOT NULL,
  `CODIGOSUCURSAL` varchar(45) NOT NULL,
  `CONDICIONCUENTA` varchar(15) NOT NULL,
  PRIMARY KEY (`NUMEROCUENTA`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuentas`
--

/*!40000 ALTER TABLE `cuentas` DISABLE KEYS */;
INSERT INTO `cuentas` (`NUMEROCUENTA`,`FECHAAPERTURA`,`TIPOCUENTA`,`TIPOMONEDA`,`SALDOCUENTA`,`IDENTIFICACION`,`MONTOAPERTURA`,`CODIGOSUCURSAL`,`CONDICIONCUENTA`) VALUES 
 (6,'21/04/2019','Corriente','$',300,'123',500,'2','Activa'),
 (7,'24/04/2019','Corriente','¢',250000,'234',200000,'1','Activa'),
 (8,'24/04/2019','Ahorro','¢',300000,'123',250000,'1','Activa'),
 (9,'24/04/2019','Ahorro','$',0,'234',500,'2','Activa'),
 (10,'24/04/2019','Corriente','$',500,'234',500,'1','Activa');
/*!40000 ALTER TABLE `cuentas` ENABLE KEYS */;


--
-- Definition of table `funciones`
--

DROP TABLE IF EXISTS `funciones`;
CREATE TABLE `funciones` (
  `CODIGONIVEL` int(10) unsigned NOT NULL,
  `REGISTRARSUCURSAL` int(10) unsigned NOT NULL,
  `REGISTRARCLIENTES` int(10) unsigned NOT NULL,
  `REGISTRARCUENTAS` int(10) unsigned NOT NULL,
  `REGISTRARTRANSACCION` int(10) unsigned NOT NULL,
  `MODIFICARSUCURSAL` int(10) unsigned NOT NULL,
  `MODIFICARCLIENTE` int(10) unsigned NOT NULL,
  `ELIMINARCUENTA` int(10) unsigned NOT NULL,
  `ELIMINARTRANSACCION` int(10) unsigned NOT NULL,
  `ACTIVARCUENTA` int(10) unsigned NOT NULL,
  `TODASCONSULTAS` int(10) unsigned NOT NULL,
  `TODASREPORTES` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `funciones`
--

/*!40000 ALTER TABLE `funciones` DISABLE KEYS */;
INSERT INTO `funciones` (`CODIGONIVEL`,`REGISTRARSUCURSAL`,`REGISTRARCLIENTES`,`REGISTRARCUENTAS`,`REGISTRARTRANSACCION`,`MODIFICARSUCURSAL`,`MODIFICARCLIENTE`,`ELIMINARCUENTA`,`ELIMINARTRANSACCION`,`ACTIVARCUENTA`,`TODASCONSULTAS`,`TODASREPORTES`) VALUES 
 (1,1,1,1,1,1,1,1,1,1,1,1),
 (2,1,1,1,1,1,1,0,0,0,0,0),
 (3,1,1,1,1,1,1,0,0,0,1,1);
/*!40000 ALTER TABLE `funciones` ENABLE KEYS */;


--
-- Definition of table `movimientos`
--

DROP TABLE IF EXISTS `movimientos`;
CREATE TABLE `movimientos` (
  `NUMEROCUENTA` int(10) unsigned NOT NULL,
  `FECHATRANSACCION` varchar(45) NOT NULL,
  `TIPOTRANSACCION` varchar(45) NOT NULL,
  `MONTOTRANSACCION` varchar(45) NOT NULL,
  `NOMBREDELRESPONSABLE` varchar(45) NOT NULL,
  `DETALLETRANSACCION` varchar(60) NOT NULL,
  `NUMEROMOVIMIENTO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`NUMEROMOVIMIENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movimientos`
--

/*!40000 ALTER TABLE `movimientos` DISABLE KEYS */;
INSERT INTO `movimientos` (`NUMEROCUENTA`,`FECHATRANSACCION`,`TIPOTRANSACCION`,`MONTOTRANSACCION`,`NOMBREDELRESPONSABLE`,`DETALLETRANSACCION`,`NUMEROMOVIMIENTO`) VALUES 
 (6,'24/04/2019','Deposito','100.0','Carlos','Depósito',1),
 (6,'24/04/2019','Retiro','300.0','Esteban','Materiales',2),
 (7,'24/04/2019','Deposito','50000.0','Ivonne','Depósito',3),
 (8,'24/04/2019','Deposito','50000.0','Ivonne','Deposito',4),
 (9,'24/04/2019','Retiro','500.0','Melissa','Retiro ',5);
/*!40000 ALTER TABLE `movimientos` ENABLE KEYS */;


--
-- Definition of table `niveles`
--

DROP TABLE IF EXISTS `niveles`;
CREATE TABLE `niveles` (
  `CODIGONIVEL` int(10) unsigned NOT NULL,
  `NOMBRENIVEL` varchar(45) NOT NULL,
  PRIMARY KEY (`CODIGONIVEL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `niveles`
--

/*!40000 ALTER TABLE `niveles` DISABLE KEYS */;
INSERT INTO `niveles` (`CODIGONIVEL`,`NOMBRENIVEL`) VALUES 
 (1,'Administrador'),
 (2,'OPERACIONAL'),
 (3,'FINAL');
/*!40000 ALTER TABLE `niveles` ENABLE KEYS */;


--
-- Definition of table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE `sucursales` (
  `CODIGOSUCURSAL` varchar(20) NOT NULL,
  `NOMBRESUCURSAL` varchar(45) NOT NULL,
  `FECHAREGISTRO` varchar(30) NOT NULL,
  `DIRECCION` varchar(45) NOT NULL,
  `TELEFONO` varchar(30) NOT NULL,
  PRIMARY KEY (`CODIGOSUCURSAL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sucursales`
--

/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
INSERT INTO `sucursales` (`CODIGOSUCURSAL`,`NOMBRESUCURSAL`,`FECHAREGISTRO`,`DIRECCION`,`TELEFONO`) VALUES 
 ('1','Heredia','21/04/2019','Paseo las Flores','22351211'),
 ('2','Cartago','21/04/2019','Cartago Centro','27375787');
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;


--
-- Definition of table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `USERNAME` varchar(50) NOT NULL,
  `CONTRASENA` varchar(45) NOT NULL,
  `CODIGONIVEL` varchar(45) NOT NULL,
  `IDENTIFICACION` varchar(45) NOT NULL,
  `CONDICIONUSUARIO` varchar(45) NOT NULL,
  `NOMBRE` varchar(45) NOT NULL,
  PRIMARY KEY (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`USERNAME`,`CONTRASENA`,`CODIGONIVEL`,`IDENTIFICACION`,`CONDICIONUSUARIO`,`NOMBRE`) VALUES 
 ('root','root','1','123','Activa','Ivonne');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
