//en esta clase se van a desarrollar todos los procedimientos que se van a ejcetuar en 
// todos los diferentes reportes del programa
package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
import javax.swing.table.DefaultTableModel;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloReportes 
{
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos(); 
    
    //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargartablacuentasxcliente(javax.swing.JTable jTable1, String ide)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM cuentas INNER JOIN clientes ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION WHERE clientes.IDENTIFICACION='"+ide+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                    
                    jTable1.setValueAt(rs.getString("numerocuenta"), linea, 0);
                    jTable1.setValueAt(rs.getString("NOMBREcliente"), linea, 1);
                    jTable1.setValueAt(rs.getString("tipocuenta"), linea, 2);
                    jTable1.setValueAt(rs.getString("tipomoneda"), linea, 3);
                    jTable1.setValueAt(rs.getString("saldocuenta"), linea, 4);
                    jTable1.setValueAt(rs.getString("codigosucursal"), linea, 5);
                    jTable1.setValueAt(rs.getString("condicioncuenta"), linea, 6);
                    linea++;
                    
                    if(linea>=4)
                    {
                        agregarnuevafila(jTable1);
                    }
                    rs.next();
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }  
    }

    public void cargartablacuentas(javax.swing.JTable jTable1)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM cuentas INNER JOIN clientes ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                    
                    jTable1.setValueAt(rs.getString("numerocuenta"), linea, 0);
                    jTable1.setValueAt(rs.getString("NOMBREcliente"), linea, 1);
                    jTable1.setValueAt(rs.getString("identificacion"), linea, 2);
                    jTable1.setValueAt(rs.getString("tipocuenta"), linea, 3);
                    jTable1.setValueAt(rs.getString("tipomoneda"), linea, 4);
                    jTable1.setValueAt(rs.getString("saldocuenta"), linea, 5);
                    jTable1.setValueAt(rs.getString("codigosucursal"), linea, 6);
                    jTable1.setValueAt(rs.getString("condicioncuenta"), linea, 7);
                    linea++;
                    
                    if(linea>=4)
                    {
                        agregarnuevafila(jTable1);
                    }
                    rs.next();
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }  
    }

    
    //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargartablacuentasxtipo(javax.swing.JTable jTable1, String tipo)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM cuentas INNER JOIN clientes ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION WHERE tipocuenta = '"+tipo+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                if(tipo.equals(rs.getString("tipocuenta")))
                {
                    
                    jTable1.setValueAt(rs.getString("numerocuenta"), linea, 0);
                    jTable1.setValueAt(rs.getString("NOMBREcliente"), linea, 1);
                    jTable1.setValueAt(rs.getString("identificacion"), linea, 2);
                    jTable1.setValueAt(rs.getString("tipomoneda"), linea, 3);
                    jTable1.setValueAt(rs.getString("saldocuenta"), linea, 4);
                    jTable1.setValueAt(rs.getString("codigosucursal"), linea, 5);
                    jTable1.setValueAt(rs.getString("condicioncuenta"), linea, 6);
                    linea++;
                    
                    if(linea>=4)
                    {
                        agregarnuevafila(jTable1);
                    }
                    rs.next();
                }
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }  
    }

      //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargartablacuentasxmoneda(javax.swing.JTable jTable1, String tipo)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM cuentas INNER JOIN clientes ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION WHERE tipomoneda = '"+tipo+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                    if(tipo.equals(rs.getString("tipomoneda")))
                    {
                    
                        jTable1.setValueAt(rs.getString("numerocuenta"), linea, 0);
                        jTable1.setValueAt(rs.getString("nombrecliente"), linea, 1);
                        jTable1.setValueAt(rs.getString("identificacion"), linea, 2);
                        jTable1.setValueAt(rs.getString("tipocuenta"), linea, 3);
                        jTable1.setValueAt(rs.getString("saldocuenta"), linea, 4);
                        jTable1.setValueAt(rs.getString("codigosucursal"), linea, 5);
                        jTable1.setValueAt(rs.getString("condicioncuenta"), linea, 6);
                        linea++;
                    
                        if(linea>=4)
                        {
                            agregarnuevafila(jTable1);
                        }
                        rs.next();
                    }
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        } 
    }
    
     
      //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargartablacuentasxtipos(javax.swing.JTable jTable1, String tipo,String mon)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM cuentas INNER JOIN clientes ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION WHERE cuentas.tipocuenta='"+tipo+"' AND cuentas.tipomoneda='"+mon+"'";
            //String query = "SELECT * FROM cuentas INNER JOIN clientes ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION WHERE clientes.IDENTIFICACION='"+ide+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                    if((tipo.equals(rs.getString("tipocuenta")))&&(mon.equals(rs.getString("tipomoneda"))))
                    {
                    
                        jTable1.setValueAt(rs.getString("numerocuenta"), linea, 0);
                        jTable1.setValueAt(rs.getString("NOMBREcliente"), linea, 1);
                        jTable1.setValueAt(rs.getString("identificacion"), linea, 2);
                        jTable1.setValueAt(rs.getString("saldocuenta"), linea, 3);
                        jTable1.setValueAt(rs.getString("codigosucursal"), linea, 4);
                        jTable1.setValueAt(rs.getString("condicioncuenta"), linea, 5);
                        linea++;
                    
                        if(linea>=4)
                        {
                            agregarnuevafila(jTable1);
                        }
                        rs.next();
                    }
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        } 
    }
     
     
//En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
public void cargartablamovimientosxcliente(javax.swing.JTable jTable1, String cuenta)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM movimientos INNER JOIN clientes INNER JOIN cuentas ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION AND cuentas.numerocuenta=movimientos.numerocuenta";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {           
                    if(cuenta.equals(rs.getString("numerocuenta")))
                    {
                    
                        jTable1.setValueAt(rs.getString("numeromovimiento"), linea, 0);
                        jTable1.setValueAt(rs.getString("tipotransaccion"), linea, 1);
                        jTable1.setValueAt(rs.getString("montotransaccion"), linea, 2);
                        jTable1.setValueAt(rs.getString("nombrecliente"), linea, 3);
                        jTable1.setValueAt(rs.getString("identificacion"), linea, 4);
                        jTable1.setValueAt(rs.getString("tipocuenta"), linea, 5);
                        jTable1.setValueAt(rs.getString("tipomoneda"), linea, 6);
                        jTable1.setValueAt(rs.getString("saldocuenta"), linea, 7);
                        linea++;
                    
                        if(linea>=4)
                        {
                            agregarnuevafila(jTable1);
                        }
                        rs.next();
                    }
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        } 
    }
     
                //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargartablaentrefechas(javax.swing.JTable jTable1, String fech1,String fech2)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM movimientos INNER JOIN clientes INNER JOIN cuentas ON cuentas.IDENTIFICACION=clientes.IDENTIFICACION AND cuentas.numerocuenta=movimientos.numerocuenta WHERE movimientos.fechatransaccion BETWEEN '"+fech1+"' AND  '"+fech2+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                    {
                    
                        jTable1.setValueAt(rs.getString("numeromovimiento"), linea, 0);
                        jTable1.setValueAt(rs.getString("tipotransaccion"), linea, 1);
                        jTable1.setValueAt(rs.getString("montotransaccion"), linea, 2);
                        jTable1.setValueAt(rs.getString("nombrecliente"), linea, 3);
                        jTable1.setValueAt(rs.getString("identificacion"), linea, 4);
                        jTable1.setValueAt(rs.getString("tipocuenta"), linea, 5);
                        jTable1.setValueAt(rs.getString("tipomoneda"), linea, 6);
                        jTable1.setValueAt(rs.getString("saldocuenta"), linea, 7);
                        linea++;
                    
                        if(linea>=4)
                        {
                            agregarnuevafila(jTable1);
                        }
                        rs.next();
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e+"pn");
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        } 
    }
     
//En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
public void cargartablatransacciontipo(javax.swing.JTable jTable1, String tipo)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM movimientos INNER JOIN cuentas ON movimientos.NUMEROCUENTA = "
                         + "cuentas.NUMEROCUENTA WHERE '"+tipo+"' = tipotransaccion";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {           
                    if(tipo.equals(rs.getString("tipotransaccion")))
                    {
                    
                        jTable1.setValueAt(rs.getString("numeromovimiento"), linea, 0);
                        jTable1.setValueAt(rs.getString("tipotransaccion"), linea, 1);
                        jTable1.setValueAt(rs.getString("montotransaccion"), linea, 2);
                        jTable1.setValueAt(rs.getString("nombredelresponsable"), linea, 3);
                        jTable1.setValueAt(rs.getString("identificacion"), linea, 4);
                        jTable1.setValueAt(rs.getString("tipocuenta"), linea, 5);
                        jTable1.setValueAt(rs.getString("tipomoneda"), linea, 6);
                        jTable1.setValueAt(rs.getString("saldocuenta"), linea, 7);
                        linea++;
                    
                        if(linea>=4)
                        {
                            agregarnuevafila(jTable1);
                        }
                        rs.next();
                    }
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        } 
    }

          
    public void cargartablausuarios(javax.swing.JTable jTable1)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM usuarios INNER JOIN niveles ON usuarios.codigonivel=niveles.codigonivel";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                    
                    jTable1.setValueAt(rs.getString("username"), linea, 0);
                    jTable1.setValueAt(rs.getString("identificacion"), linea, 1);
                    jTable1.setValueAt(rs.getString("nombre"), linea, 2);
                    jTable1.setValueAt(rs.getString("condicionusuario"), linea, 3);
                    jTable1.setValueAt(rs.getString("codigonivel"), linea, 4);
                    jTable1.setValueAt(rs.getString("nombrenivel"), linea, 5);
                    jTable1.setValueAt(rs.getString("funciones"), linea, 6);
                    linea++;
                    
                    if(linea>=4)
                    {
                        agregarnuevafila(jTable1);
                    }
                    rs.next();
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }  
    }
    
    
               //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargarbitacoraentrefechas(javax.swing.JTable jTable1, String fech1,String fech2)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM bitacora INNER JOIN usuarios INNER JOIN niveles ON bitacora.usuario=usuarios.username AND usuarios.codigonivel=niveles.codigonivel WHERE bitacora.fechamovimiento BETWEEN '"+fech1+"' AND  '"+fech2+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                    {
                    
                        jTable1.setValueAt(rs.getString("numeromovimiento"), linea, 0);
                        jTable1.setValueAt(rs.getString("usuario"), linea, 1);
                        jTable1.setValueAt(rs.getString("transaccionrealizada"), linea, 2);
                        jTable1.setValueAt(rs.getString("codigonivel"), linea, 3);
                        jTable1.setValueAt(rs.getString("nombre"), linea, 4);
                        jTable1.setValueAt(rs.getString("identificacion"), linea, 5);
                        jTable1.setValueAt(rs.getString("nombrenivel"), linea, 6);
                        linea++;
                    
                        if(linea>=4)
                        {
                            agregarnuevafila(jTable1);
                        }
                        rs.next();
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        } 
    }

     //En este procedimiento se va a imprimir todos los datos de cuentas de un cliente
     public void cargarbitacoraxusuario(javax.swing.JTable jTable1, String name)
    {
        int linea=0;
        try //Intente hacer
        { 
           cn.conectarbase();
            
            String query = "SELECT * FROM bitacora INNER JOIN usuarios INNER JOIN niveles ON bitacora.usuario=usuarios.username AND usuarios.codigonivel=niveles.codigonivel WHERE usuarios.username='"+name+"'";
            cn.stmt.execute(query);
            ResultSet rs = cn.stmt.executeQuery(query);
            rs.first();    
            
            if (rs!=null)
            {
                do
                {
                    
                    jTable1.setValueAt(rs.getString("numeromovimiento"), linea, 0);
                    jTable1.setValueAt(rs.getString("transaccionrealizada"), linea, 1);
                    jTable1.setValueAt(rs.getString("nombrenivel"), linea, 2);
                    jTable1.setValueAt(rs.getString("fechamovimiento"), linea, 3);
                    jTable1.setValueAt(rs.getString("identificacion"), linea, 4);
                    linea++;
                    
                    if(linea>=4)
                    {
                        agregarnuevafila(jTable1);
                    }
                    rs.next();
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }  
    }


    public void agregarnuevafila(javax.swing.JTable Table)
    {
        DefaultTableModel temp = (DefaultTableModel) Table.getModel();
        Object nuevo[] = {"","","","","","","",""};
        //Inserte la nueva fila al objeto correpondiente
        temp.addRow(nuevo);
    }
}
