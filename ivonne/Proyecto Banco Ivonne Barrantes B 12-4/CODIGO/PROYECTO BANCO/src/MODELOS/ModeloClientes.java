//en esta clase se van a desarrollar todos los procedimientos que se van 
//a realizar en las diversas funciones de vistas clientes
package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloClientes 
{
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos();  
    
    //Función que permite buscar una identificación en la 
    //tablaempleados
    public int buscacliente(String ide)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM clientes WHERE identificacion = '"+ide+"'";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if (ide.equals(rs.getString("identificacion")))
                    enco = 1;
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }   
    
    //Función que permite ingresar un registro en la tablasucursales
    public boolean ingresarclientes(String ide,String nom, String tef, 
            String dir, String mail, String fecha,String cod)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO clientes (identificacion,nombrecliente,telefono,direccion,correo,fecharegistro,codigosucursal) VALUES ('"+ide+"','"+nom+"','"+tef+"','"+dir+"','"+mail+"','"+fecha+"','"+cod+"')";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }
    
     //procedimiento que permite cargar los clientes registrados en la tabla combo
    public void cargarcomboide(javax.swing.JComboBox combo)
    {

        
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM clientes";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo IDENTIFICACION al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("identificacion"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }

     public void mostrarcliente(String ide, javax.swing.JTextField nom,
            javax.swing.JTextField tef,javax.swing.JTextField dir,
            javax.swing.JTextField mail,javax.swing.JTextField fecha,
             javax.swing.JTextField cod)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM clientes WHERE identificacion = '"+ide+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(ide.equals(rs.getString("identificacion")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    nom.setText(rs.getString("Nombrecliente"));
                    tef.setText(rs.getString("telefono"));
                    dir.setText(rs.getString("direccion"));
                    mail.setText(rs.getString("correo"));
                    fecha.setText(rs.getString("fecharegistro"));
                    cod.setText(rs.getString("codigosucursal"));
                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }//termina mostar datos

     //esta es la funcion que permite modificar los datos  de un cliente
     public boolean modificarcliente(String pide,String pnom, String ptef, String pdir, String pmail,
            String pfecha,String pcod)
     {
         try
         {
            cn.conectarbase();
            //Esta es la instrucción en lenguaje SQL que se va a ejecutar
            //cuando se va a insertar un nuevo registro en la tabla de la BD
            //SQL = Structured Query Languaje
            String query = "UPDATE clientes SET telefono='"+ptef+"',direccion='"+pdir+"',correo='"+pmail+"',FechaRegistro='"+pfecha+"',codigosucursal='"+pcod+"',nombrecliente='"+pnom+"' WHERE identificacion = '"+pide+"'";
            //String query = "UPDATE clientes SET telefono = '"+ptef+"' , direccion = '"+pdir+"' , correo = '"+pmail+"' , FechaRegistro = '"pfecha+"' , codigosucursal = '"+pcod+"' , nombrecliente = '"+pnom+"' WHERE identificacion = '"+pide+"' ";

            cn.stmt.execute(query);
        }
      catch(Exception e)
        {
            //Envía a imprimir la Excepción que no se pudo
            //hacer en el Try
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return true;
     }

}
