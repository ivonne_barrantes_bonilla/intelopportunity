//la clase donde se van a desarrollar todos los procedimiento que se van 
//a a utilizar en departamento de vistamovimientos

package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloMovimientos 
{
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos(); 
    
    //Función que permite ingresar un registro en la tabla movimientos
    public boolean ingresarmovimiento(int num,String fecha, String tipo, 
            double monto, String nom, String detalle)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO movimientos (numerocuenta,fechatransaccion,tipotransaccion,montotransaccion,nombredelresponsable,detalletransaccion) VALUES ("+num+",'"+fecha+"','"+tipo+"',"+monto+",'"+nom+"','"+detalle+"')";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }

    //procedimiento que permite cargar los clientes registrados en la tabla combo
    public void cargarcombomovimiento(javax.swing.JComboBox combo)
    {

        
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM movimientos";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo IDENTIFICACION al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("numeromovimiento"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }
    
     //Función que permite buscar una identificación en la 
    //tablaempleados
    public int buscamovimiento(String cod)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM movimientos WHERE numeromovimiento = '"+cod+"'";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if (cod.equals(rs.getString("numeromovimiento")))
                    enco = 1;
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }        

    public void mostrarmovimiento(String cod, javax.swing.JTextField num,
            javax.swing.JTextField fecha,javax.swing.JTextField tipo,
            javax.swing.JTextField monto,javax.swing.JTextField nom,
            javax.swing.JTextField detalle)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM movimientos WHERE numeromovimiento = '"+cod+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(cod.equals(rs.getString("numeromovimiento")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    num.setText(rs.getString("numerocuenta"));
                    fecha.setText(rs.getString("fechatransaccion"));
                    tipo.setText(rs.getString("tipotransaccion"));
                    monto.setText(rs.getString("montotransaccion"));
                    nom.setText(rs.getString("nombredelresponsable"));
                    detalle.setText(rs.getString("detalletransaccion"));
                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        
    }
    
    
        //Función que recibe una identificación del cliente que se quiere eliminar
    public boolean eliminarmovimiento(int np)
    {
        boolean borrado= true;
        
        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            cn.stmt.execute("DELETE FROM movimientos WHERE numeromovimiento="+np+"");
        }   
        catch(Exception e)
        {

            System.out.println(e);
            borrado=false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }       
            catch(Exception e)
            { }
        }
        return borrado;
    }
    
    
     public void quitasaldo(int num ,double dinero)
    {
        try
        {
            cn.conectarbase();
            cn.stmt.execute("UPDATE cuentas SET saldocuenta = saldocuenta- "+dinero+" WHERE numerocuenta = "+num+"");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
    }
    
    //procedimiento que hace que el saldo disminuya dependiendo de lo que el cliente ingreso
    public void regresasaldo(int num ,double dinero)
    {
        try
        {
            cn.conectarbase();
            cn.stmt.execute("UPDATE cuentas SET saldocuenta = saldocuenta+ "+dinero+" WHERE numerocuenta = "+num+"");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
    }

}
