// en esta clase se van a desaarrolllar todos los procedimientos del proyecto
//refenrente a la parte de la cuentas de ususario
package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloCuentas 
{
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos();      
    
    //Función que permite ingresar un registro en la tablasCUENTAS
    public boolean ingresarcuentas(String fecha,String cuenta, String moneda, 
            double saldo, String ide, double monto,String cod,String cond)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO cuentas (fechaapertura,tipocuenta,tipomoneda,saldocuenta,identificacion,montoapertura,codigosucursal,condicioncuenta) VALUES ('"+fecha+"','"+cuenta+"','"+moneda+"','"+saldo+"','"+ide+"','"+monto+"','"+cod+"','"+cond+"')";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }

     public void mostrarcuenta(int num, javax.swing.JTextField fecha,
            javax.swing.JTextField cuenta,javax.swing.JTextField moneda,
            javax.swing.JTextField saldo,javax.swing.JTextField ide,
             javax.swing.JTextField cod,javax.swing.JTextField cond)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM cuentas WHERE numerocuenta = "+num+"";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(num==(rs.getInt("numerocuenta")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    fecha.setText(rs.getString("fechaapertura"));
                    cuenta.setText(rs.getString("tipocuenta"));
                    moneda.setText(rs.getString("tipomoneda"));
                    saldo.setText(rs.getString("saldocuenta"));
                    ide.setText(rs.getString("identificacion"));
                    cod.setText(rs.getString("codigosucursal"));
                    cond.setText(rs.getString("condicioncuenta"));
                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }//termina mostar datos
    
    public int buscacuenta(int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM cuentas WHERE numerocuenta = "+num+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("numerocuenta")))
                    enco = 1;
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }   
    
       //procedimiento que permite cargar las cuentas registrados en la tabla combo
    public void cargarcombocuenta(javax.swing.JComboBox combo)
    {

        
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM cuentas";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo IDENTIFICACION al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("numerocuenta"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }
    
           //procedimiento que permite cargar las cuentas registrados en la tabla combo
    public void cargarcombotrans(javax.swing.JComboBox combo)
    {

        String activo="Activa";
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM cuentas where condicioncuenta='"+activo+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo IDENTIFICACION al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("numerocuenta"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }

        public void mostrarcuentaad(int num, javax.swing.JTextField ide,
            javax.swing.JTextField fecha,javax.swing.JTextField cuenta,
            javax.swing.JTextField moneda,javax.swing.JTextField cod,
            javax.swing.JTextField saldo, javax.swing.JTextField est)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM cuentas WHERE numerocuenta = "+num+"";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(num==(rs.getInt("numerocuenta")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    ide.setText(rs.getString("identificacion"));
                    fecha.setText(rs.getString("fechaapertura"));
                    cuenta.setText(rs.getString("tipocuenta"));
                    moneda.setText(rs.getString("tipomoneda"));
                    cod.setText(rs.getString("codigosucursal"));
                    saldo.setText(rs.getString("saldocuenta"));
                    est.setText(rs.getString("condicioncuenta"));
                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }//termina mostar datos

        //este procedimiento se utilizara para modificar el estado de las cuentas a
        //activas y desactivas
    public boolean modificarestado(int num,String estado)
     {
         try
         {
            cn.conectarbase();
            //Esta es la instrucción en lenguaje SQL que se va a ejecutar
            //cuando se va a insertar un nuevo registro en la tabla de la BD
            //SQL = Structured Query Languaje
            String query = "UPDATE cuentas SET condicioncuenta='"+estado+"' where numerocuenta='"+num+"'";
            cn.stmt.execute(query);
        }
      catch(Exception e)
        {
            //Envía a imprimir la Excepción que no se pudo
            //hacer en el Try
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return true;
     }

     public void mostrarsaldo(int num,javax.swing.JTextField saldo,javax.swing.JTextField moneda)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM cuentas WHERE numerocuenta = "+num+"";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(num==(rs.getInt("numerocuenta")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    saldo.setText(rs.getString("saldocuenta"));
                    moneda.setText(rs.getString("tipomoneda"));
                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
    }

    public void depositasaldo(int num ,double dinero)
    {
        try
        {
            cn.conectarbase();
            cn.stmt.execute("UPDATE cuentas SET saldocuenta = saldocuenta+ "+dinero+" WHERE numerocuenta = "+num+"");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
    }
    
    //procedimiento que hace que el saldo disminuya dependiendo de lo que el cliente ingreso
    public void retirasaldo(int num ,double dinero)
    {
        try
        {
            cn.conectarbase();
            cn.stmt.execute("UPDATE cuentas SET saldocuenta = saldocuenta- "+dinero+" WHERE numerocuenta = "+num+"");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
    }


    //funcion que recibe una identificacion del cliente como parametro
    //del qcliente que se quiera eliminar 
    public boolean eliminar(int num)
    {
        boolean borrado = true;
                
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "DELETE FROM Cuentas WHERE numerocuenta = "+num+"";
            cn.stmt.execute(query);
            
        }   
        catch(Exception e)
        {
            System.out.println(e);
            borrado = false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return borrado;
    }
    
    //funcion que recibe una identificacion del cliente como parametro
    //del qcliente que se quiera eliminar 
    public boolean eliminarmovimientos(int num)
    {
        boolean borrado = true;
                
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "DELETE  FROM movimientos WHERE numerocuenta = "+num+"";
            cn.stmt.execute(query);
            
        }   
        catch(Exception e)
        {
            System.out.println(e);
            borrado = false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return borrado;
    }
//Función que permite buscar una identificación en la 
    public int validaractivo(int num)
    {
        int enco = 0;
        String activo="Activa";
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM cuentas WHERE numerocuenta = "+num+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if (activo.equals(rs.getString("condicioncuenta")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }  

    

}
