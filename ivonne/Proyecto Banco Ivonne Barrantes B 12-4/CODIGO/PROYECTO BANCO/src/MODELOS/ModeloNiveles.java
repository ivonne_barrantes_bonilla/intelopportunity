//En esta clase se van a realizar todos los procedimiento de la parte de mantenimiento
//de niveles en el proyecto
package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloNiveles 
{
    
    
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos(); 
    
    //Función que permite buscar una identificación en la 
    //tablaempleados
    public int buscacod(int cod)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM niveles WHERE codigonivel = "+cod+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if (cod==(rs.getInt("codigonivel")))
                    enco = 1;
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }   

    //Función que permite ingresar un registro en la tablasucursales
    public boolean ingresarnivel(int cod,String nom)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO niveles (codigonivel,nombrenivel) VALUES ("+cod+",'"+nom+"')";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }
    
    public void mostrarniveles(String cod, javax.swing.JTextField nom,
            javax.swing.JTextField funciones)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM niveles WHERE CODIGONIVEL = '"+cod+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(cod.equals(rs.getString("Codigonivel")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    nom.setText(rs.getString("Nombrenivel"));
                    funciones.setText(rs.getString("funciones"));

                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }
     //procedimiento que permite cargar los clientes registrados en la tabla combo
    public void cargarcomboniveles(javax.swing.JComboBox combo)
    {

        
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM niveles";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo IDENTIFICACION al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("CODIGONIVEL"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }
    
     //Función que permite ingresar un registro en la tablasucursales
    public boolean ingresarfunciones(int cod,int rsuc,int rclien, int rcuen,int rtrans,int msuc,int mclien,int ecuen,int etrans,int adcuen,int tcons,int trepor)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO funciones (codigonivel,REGISTRARSUCURSAL,registrarclientes,registrarcuentas,registrartransaccion,modificarsucursal,modificarcliente,eliminarcuenta,eliminartransaccion,activarcuenta,todasconsultas,todasreportes) VALUES ("+cod+","+rsuc+","+rclien+","+rcuen+","+rtrans+","+msuc+","+mclien+","+ecuen+","+etrans+","+adcuen+","+tcons+","+trepor+")";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }

            
   //en este procedimiento se va a preguntar en la base de datos las opciones que puede realizar ese nivel
     public void mostrarfunciones(String username, javax.swing.JTextField rsuc,
            javax.swing.JTextField rclien,javax.swing.JTextField rcuen,
            javax.swing.JTextField rtrans,javax.swing.JTextField msuc,
            javax.swing.JTextField mclien,javax.swing.JTextField ecuen,
            javax.swing.JTextField etrans,javax.swing.JTextField acuen,
            javax.swing.JTextField tconsul,javax.swing.JTextField trepor)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM funciones INNER JOIN usuarios ON funciones.codigonivel=usuarios.codigonivel WHERE usuarios.username='"+username+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(username.equals(rs.getString("username")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    rsuc.setText(rs.getString("registrarsucursal"));
                    rclien.setText(rs.getString("registrarclientes"));
                    rcuen.setText(rs.getString("registrarcuentas"));
                    rclien.setText(rs.getString("registrartransaccion"));                   
                    msuc.setText(rs.getString("modificarsucursal"));
                    mclien.setText(rs.getString("modificarcliente"));
                    ecuen.setText(rs.getString("eliminarcuenta"));
                    etrans.setText(rs.getString("eliminartransaccion"));
                    acuen.setText(rs.getString("activarcuenta"));
                    tconsul.setText(rs.getString("todasconsultas"));
                    trepor.setText(rs.getString("todasreportes"));

                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }

}
