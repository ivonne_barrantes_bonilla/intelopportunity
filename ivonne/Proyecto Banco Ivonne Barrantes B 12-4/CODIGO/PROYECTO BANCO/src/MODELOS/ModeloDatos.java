//En esta clase se van a realizar todos los procedimientos del programa
package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloDatos 
{
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos();
    
    
    
    //Función que permite buscar una identificación en la 
    //tablaempleados
    public int buscasucursal(String cod)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM sucursales WHERE codigosucursal = '"+cod+"'";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if (cod.equals(rs.getString("codigosucursal")))
                    enco = 1;
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }        

    //Función que permite ingresar un registro en la tablasucursales
    public boolean ingresarsucursales(String cod, String nom, 
            String fecha, String dir, String tef)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO sucursales (codigosucursal,nombresucursal,fecharegistro,direccion,telefono) VALUES ('"+cod+"','"+nom+"','"+fecha+"','"+dir+"','"+tef+"')";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }
    
    //procedimiento que permite cargar los clientes registrados en la tabla combo
    public void cargarcombosucursales(javax.swing.JComboBox combo)
    {

        
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM SUCURSALES";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo IDENTIFICACION al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("CODIGOSUCURSAL"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }
    public void mostrarsucursales(String cod, javax.swing.JTextField nom,
            javax.swing.JTextField fecha,javax.swing.JTextField tef,
            javax.swing.JTextField dir)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM SUCURSALES WHERE CODIGOSUCURSAL = '"+cod+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(cod.equals(rs.getString("Codigosucursal")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    nom.setText(rs.getString("Nombresucursal"));
                    fecha.setText(rs.getString("fecharegistro"));
                    dir.setText(rs.getString("direccion"));
                    tef.setText(rs.getString("telefono"));
                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }//termina mostar datos
    
    //esta es la funcion que permite modificar los datos  de un cliente
     public boolean modificarsucursal(String pcod, String pnom, String pfecha, String pdir,
            String ptef)
     {
         try
         {
            cn.conectarbase();
            //Esta es la instrucción en lenguaje SQL que se va a ejecutar
            //cuando se va a insertar un nuevo registro en la tabla de la BD
            //SQL = Structured Query Languaje
            String query = "UPDATE Sucursales SET Nombresucursal='"+pnom+"',FechaRegistro='"+pfecha+"',Direccion='"+pdir+"',telefono='"+ptef+"' WHERE codigosucursal='"+pcod+"'";
            cn.stmt.execute(query);
        }
      catch(Exception e)
        {
            //Envía a imprimir la Excepción que no se pudo
            //hacer en el Try
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return true;
     }


}
