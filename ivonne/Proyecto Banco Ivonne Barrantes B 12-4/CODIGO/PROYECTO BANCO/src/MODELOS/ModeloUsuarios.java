
package MODELOS;

import java.sql.*;
import BASEDATOS.ConexionBaseDatos;
//Aquí importa la clase conexionBaseDatos del paquete BASEDATOS

public class ModeloUsuarios 
{
    //Instancia la clase ConexionBaseDatos
    ConexionBaseDatos cn = new ConexionBaseDatos(); 
    
    public static String ploginusuario;
    public static String pnivelusuario;
    
    
    //Función que permite buscar una identificación en la 
    //tablaempleados
    public int buscausuario(String login, String pass)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM usuarios WHERE username = '"+login+"' AND contrasena = '"+pass+"'";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if ((login.equals(rs.getString("username")))&&(pass.equals(rs.getString("contrasena"))))
                {
                    enco = 1;
                    ploginusuario=login;                   
                    if(rs.getInt("codigonivel")==1)
                        pnivelusuario = "ADMINISTRADOR";
                    else
                        if(rs.getInt("codigonivel")!=1)
                            pnivelusuario = "OPERACIONAL";
                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }  
    
    //Función que permite ingresar un registro en la tablasCUENTAS
    public boolean ingresarusuario(String user,String pass, String cod, String ide,String cond,String nom)
    {
        try
        {
            cn.conectarbase();
            String query = "INSERT INTO usuarios (username,contrasena,codigonivel,identificacion,condicionusuario,nombre) VALUES ('"+user+"','"+pass+"','"+cod+"','"+ide+"','"+cond+"','"+nom+"')";
            cn.stmt.executeUpdate(query); //Ejecuta la sentencia de SQL     
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return true;
    }

    //este procedimiento busca si el nombre de usuario esta disponible o ya alguien tiene registrado
    public int buscausername(String user)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM usuarios WHERE username = '"+user+"'";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(user.equals(rs.getString("username")))
                    enco = 1;
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }   

         //procedimiento que permite cargar las cuentas registrados en la tabla combo
    public void cargarcombousuarios(javax.swing.JComboBox combo)
    {

        
        try //Intente hacer
        { 
            //Aquí va el String de Conexión a la BD
            cn.conectarbase();
            
            String query = "SELECT * FROM usuarios";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //Este ciclo permite recorrer todo el rs y agrega
                //todos los datos del camo IDENTIFICAICON al combo               
                do
                {
                    
                    //aqui carga el campo  al combo
                    //que recibe como parametro
                    combo.addItem(rs.getString("username"));
                    rs.next();
                    //mueve el puntero al siguiente registro del rs
                    
                }while(rs!=null);
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }     
    }
    
    
    public void mostrarusuario(String username, javax.swing.JTextField cod,
            javax.swing.JTextField ide,javax.swing.JTextField cond,
            javax.swing.JTextField nom)
    {

        
        try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM usuarios WHERE username = '"+username+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(username.equals(rs.getString("username")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    cod.setText(rs.getString("codigonivel"));
                    ide.setText(rs.getString("identificacion"));
                    cond.setText(rs.getString("condicionusuario"));
                    nom.setText(rs.getString("nombre"));

                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }

    }//termina mostar datos
     //esta es la funcion que permite modificar los datos  de un cliente
     public boolean modificarusuario(String pcod,String pide, String pcond, String pnom)
     {
         try
         {
            cn.conectarbase();
            //Esta es la instrucción en lenguaje SQL que se va a ejecutar
            //cuando se va a insertar un nuevo registro en la tabla de la BD
            //SQL = Structured Query Languaje
            String query = "UPDATE usuarios SET codigonivel='"+pcod+"',identificacion='"+pide+"',condicionusuario='"+pcond+"',nombre='"+pnom+"'";
            cn.stmt.execute(query);
        }
      catch(Exception e)
        {
            //Envía a imprimir la Excepción que no se pudo
            //hacer en el Try
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return true;
     }

   public boolean modificarestado(String name,String estado)
     {
         try
         {
            cn.conectarbase();
            //Esta es la instrucción en lenguaje SQL que se va a ejecutar
            //cuando se va a insertar un nuevo registro en la tabla de la BD
            //SQL = Structured Query Languaje
            String query = "UPDATE usuarios SET condicionusuario='"+estado+"' where username='"+name+"'";
            cn.stmt.execute(query);
        }
      catch(Exception e)
        {
            //Envía a imprimir la Excepción que no se pudo
            //hacer en el Try
            System.out.println(e);
            return false;
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
        return true;
     }

//Función que permite buscar una identificación en la 
    public int validaractivo(String login)
    {
        int enco = 0;
        String activo="Activa";
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM usuarios WHERE username = '"+login+"'";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if (activo.equals(rs.getString("condicionusuario")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }  
    
    public void mostrarnivel(String login,javax.swing.JTextField niv)
    {
                try //Intente hacer
        { 
            cn.conectarbase();
            
            String query = "SELECT * FROM usuarios WHERE username = '"+login+"'";
            cn.stmt.execute(query);
            
            //la variable de tipo resultset guarda a los resultados
            //despues de ejecutar una instruccion de SQL
            ResultSet rs = cn.stmt.executeQuery(query);
            //Aquí indicamos al compilador que ejecute la instrucción query
            
            rs.first();
            //ubica el puntero en el primer registro del rs
            
            //si es diferente de null quiere decir que el guardo en la
            //de la instruccion select
            if (rs!=null)
            {
                //aqui compara el parametro ide con el campo identificacion del rs
                //pq el rs tiene todos los cmapos de la tabla correpsondiente
                if(login.equals(rs.getString("username")))
                {
                    //aqui asigna a los parametros los campos de la tabla
                    niv.setText(rs.getString("codigonivel"));

                }
            }
        }   
        catch(Exception e)
        {

            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase(); //Cierra la BD
            }       
            catch(Exception e)
            { }
        }
    }
    
 
    //Esta Funcion permite para seleccionar si la opcion registrar sucursal si es 1 desbloquea la casilla
    public int rsucursal(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
            {   
                if(num==(rs.getInt("registrarsucursal")))
                {
                    enco = 1;

                }
                   
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }  

    //Esta Funcion permite para seleccionar si la opcion registrar clientes si es 1 desbloquea la casilla
    public int rclientes(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("registrarclientes")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }  

    //Esta Funcion permite para seleccionar si la opcion registrar cuentas si es 1 desbloquea la casilla
    public int rcuentas(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("registrarcuentas")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    }  
    
     //Esta Funcion permite para seleccionar si la opcion registrar transaccion si es 1 desbloquea la casilla
    public int rtrans(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("registrartransaccion")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 

     //Esta Funcion permite para seleccionar si la opcion modificar sucursal si es 1 desbloquea la casilla
    public int msucursal(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("modificarsucursal")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
     //Esta Funcion permite para seleccionar si la opcion modificar cliente si es 1 desbloquea la casilla
    public int mcliente(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("modificarcliente")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
     //Esta Funcion permite para seleccionar si la opcion eliminar cuenta si es 1 desbloquea la casilla
    public int ecuenta(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("eliminarcuenta")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
    //Esta Funcion permite para seleccionar si la opcion eliminar transaccion si es 1 desbloquea la casilla
    public int etrans(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("eliminartransaccion")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
    //Esta Funcion permite para seleccionar si la opcion activar cuenta si es 1 desbloquea la casilla
    public int acuenta(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("activarcuenta")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
    //Esta Funcion permite para seleccionar si la opcion todas las consultas si es 1 desbloquea la casilla
    public int consultas(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("todasconsultas")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
    //Esta Funcion permite para seleccionar si la opcion todas los reportes si es 1 desbloquea la casilla
    public int reportes(int nivel, int num)
    {
        int enco = 0;
        try
        {
            cn.conectarbase();
            String query = "SELECT * FROM funciones WHERE codigonivel = "+nivel+"";
            ResultSet rs = cn.stmt.executeQuery(query);
            //La variable rs va a guardar los registros que cumplan
            //la condición dada en el SELECT
            rs.first();     //Ubica primer registro del rs
            if (rs!=null)   //Quiere decir que hay datos en el rs
                if(num==(rs.getInt("todasreportes")))
                {
                    enco = 1;

                }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
        finally
        {
            try
            {
                cn.desconectarbase();
            }
            catch(Exception e)
            { }
        }
        return enco;
    } 
    
}
