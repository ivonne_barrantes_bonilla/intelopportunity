//Esta es la clase que permite hacer la conexion con la base de datos

package BASEDATOS;

import java.sql.*;

public class ConexionBaseDatos 
{
    //define parametros de base de datos 
    static String bd = "proyecto";
    static String login = "root";
    static String password = "root";
    static String url ="jdbc:mysql://localhost/"+bd;
    //En este url especificamos la direccion donde esta la bd, si
    //si por ejemplo la bd esta en un servidor, ahi escribiriamos la 
    //direccion ip correspondiente
    
    
    //La variable db se utiliza para establecer conexión con la 
    //base de datos estas variables se definen de tipo static para 
    //que se puedan utilizar en cualquier lugar de todo
    //siempre y cuando la ckase haya sudi intanciada
    public static Connection con = null;
    
    //Esta variable se utiliza para ejecutar instrucciones sobre
    //las tablas de la BD
    public static Statement stmt;

    public void conectarbase()
    {
        try 
        {
            //String o cadena de conexión que permite conectar 
            //con la DB
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, login , password);
            stmt = con.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            
            if (con!=null)
                System.out.println("Conexión a Base Datos " +url+ "Correcta..");
        }
        catch(SQLException ex)
        {
            System.out.println(ex);
        }
        catch(ClassNotFoundException ex)
        {
            System.out.println(ex);
        }
    } //Fin de la Clase 
    
    public void desconectarbase()
    {
        try
        {
               con.close();
        }
        catch(Exception ex)
         { 
             System.out.println(ex);
         }
    }
    
}