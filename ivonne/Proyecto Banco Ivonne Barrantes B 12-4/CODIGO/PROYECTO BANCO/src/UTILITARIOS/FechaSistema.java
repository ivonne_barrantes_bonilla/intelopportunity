
package UTILITARIOS;

import java.util.Date;

public class FechaSistema 
{
    String mesFix;
    //Función que permite obtener la fecha del sistema
    public String Fecha()
    {
        int dia, mes, anio;
        Date f = new Date();
        dia = f.getDate();  //Obtiene el día 
        mes = f.getMonth() + 1; 
        //Obtiene el mes y se suma pq lo genera a partir de 0
        anio = f.getYear() + 1900; 
        //Obtiene el año y se suma 1900 para 4 dígitos
        
        if(mes<10)
            mesFix ="0"+String.valueOf(mes);
        else
            mesFix=String.valueOf(mes);
        //En este return envía las variables y formato a imprimir
        return dia + "/" + mesFix + "/" + anio;
    }   
}
