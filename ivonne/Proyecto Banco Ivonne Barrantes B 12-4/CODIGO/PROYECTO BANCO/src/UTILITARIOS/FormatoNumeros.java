//esta clase se utilizara cuanto se deba utilizar el formato de dolares o colones

package UTILITARIOS;

import java.text.DecimalFormat;

public class FormatoNumeros 
{
    public DecimalFormat formatocolones = new DecimalFormat("¢0.00");
    
    public DecimalFormat formatodolares = new DecimalFormat("$0.00");
    
    public void aplicaformatocolones(javax.swing.JTextField campo, double dato)
    {
        campo.setText(""+formatocolones.format(dato));
        //asigna el campo texto que recibe como parametro
    }
    
    public void aplicaformatodolares(javax.swing.JTextField campo, double dato)
    {
        campo.setText(""+formatodolares.format(dato));
        //asigna el campo texto que recibe como parametro
    }
}
